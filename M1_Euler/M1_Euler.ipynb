{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Préambule python :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from math import sqrt, cos, sin, exp, pi\n",
    "from scipy.integrate import odeint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font size=\"+12\"><center>\n",
    "Modélisation 1 :  Méthode d' Euler\n",
    "</center></font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#  Théorie et mise en pratique de la méthode d'Euler\n",
    "\n",
    "## La méthode d'Euler\n",
    "\n",
    "La méthode d'Euler est une méthode mathématique permettant de construire une suite de valeurs, approximant la fonction solution d'une équation différentielle. Le fait de construire une suite permet d'utiliser aisément cette méthode en informatique : on dit alors qu'on **discrétise** le problème.\n",
    "\n",
    "Commençons avec le point de vue mathématique, pour une équation différentielle du premier degré, accompagnée de sa condition initale, écrite ainsi :\n",
    "$$\\begin{cases}\n",
    "y'(t) = f(y(t),t)\\ \\ \\ (E) \\\\\n",
    "y(t_i) =  y_i\n",
    "\\end{cases}$$\n",
    "\n",
    "En mathématiques, ce problème est nommé \"problème de Cauchy\". Notre but est de résoudre ce problème sur un intervalle de temps $I = [t_i,t_f]$. L'équation (E) est une équation différentielle du premier ordre (car ne faisant intervenir, comme fonction, que $y(t)$ et $y'(t)$, la dérivée première de $y(t)$), mais pas forcément linéaire (elle ne l'est que si la fonction $f$ l'est aussi). Nous allons ainsi, grâce à cette méthode, pouvoir résoudre approximativement des équations différentielles non linaires ! Le travail préliminaire à effectuer est de discrêtiser l'intervalle sur lequel nous allons résoudre cette équation.\n",
    "\n",
    "Notons $t_j$ les $n+1$ valeurs ($j \\in [\\![0,n ]\\!]$) discrètes prises par la variable $t$ sur cet intervalle : $t_0 = t_{i}, t_1, t_2, ..., t_{n-1}, t_n = t_{f}$. Le mot *discret* s'oppose ici à *continu*. En effet, la variable $t$ ne varie plus continument entre $t_i$ et $t_f$, mais par *bonds*, nommé *pas*, de valeur $h = \\frac{t_f-t_0}{n}$ ($n+1$ valeurs pour les temps $t_j$, donc $n$ pas). A chacune de ces valeurs $t_j$, nous allons faire correspondre une valeur $y_j$, valeur approchée de la fonction $y$ en $t_j$ : $y_j \\simeq y(t_j)$. Il s'agit d'une valeur approchée car cette méthode n'est pas exacte.\n",
    "\n",
    "La mtéhode d'Euler repose sur une construction itérative (proche de ce qu'on pourrait faire dans une démonstration par récurence) : on part du premier point, de coordonnées $(t_0 = t_i, y_0 = y_i)$, donné exactement par la condition initiale.\n",
    "\n",
    "![image](im/Euler1.png)\n",
    "\n",
    "On construit ensuite la suite des points de coordonnées $(t_j, y_j)$, en s'aidant de l'équation différentielle, selon le schéma indiqué par la figure ci-dessus (attention la courbe en noir est la courbe recherchée, elle n'est a priori pas connue !) :\n",
    "\n",
    "- Pour obtenir $y_1$ à partir de $y_0$, on approxime la courbe recherchée par sa tangente en $(t_0,y_0)$ : c'est effectivement ce qu'on peut faire de mieux, avec les informations dont nous disposons. La pente de cette tangente est $y'(t_0) = f(y(t_0),t_0) = f(y_0,t_0)$ d'après l'équation différentielle et on en déduit ensuite $y_1$ en \"avançant\" le temps du pas $h$ jusqu'à la prochaine valeur discrête du temps $t_1$.\n",
    "\n",
    "- De même, pour obtenir $y_2$ à partir de $y_1$, on approxime la courbe recherchée par sa tangente en $(t_1,y_1)$: la pente de cette tangente est $y'(t_1) = f(y(t_1),t_1) \\simeq f(y_1,t_1)$ d'après l'équation différentielle et on en déduit ensuite $y_2$ en \"avançant\" le temps de $h$ jusqu'au prochain pas $t_2$.\n",
    "\n",
    "- Et ainsi de suite, on répète ces étapes encore $n-2$ fois jusqu'à arriver à la fin de l'intervalle, et obtenir les $n+1$ points de coordonnées $(t_0, y_0)$, $(t_1, y_1)$, $( t_2, y_2)$, ..., $(t_{n-1}, y_{n-1})$,$(t_{n}, y_{n})$.\n",
    "\n",
    "Cherchons le liens entre deux valeurs consécutives approchées de la fonction, c'est-à-dire exprimons $y_{j}$ en fonction de $y_{j-1}$. Cela pourrait correspondre à une étape dite d'*hérédité* dans une démonstration par récurrence.\n",
    "\n",
    "![image](im/Euler2.png)  \n",
    "\n",
    "On a alors, d'après ce graphe, la pente de la tangente en $t_{j-1}$ qui vaut le taux d'accroissement $\\frac{y_{j}-y_{j-1}}{t_{j}-t_{j-1}}=\\frac{y_{j}-y_{j-1}}{h}$. Or cette pente est aussi la dérivée de la fonction en $t_{j-1}$ (connue grâce à l'équation différentielle), on obtient donc :\n",
    "$$ y'(t_{j-1}) = \\frac{y_{j}-y_{j-1}}{h}$$\n",
    "Ce qui donne pour $y_j$ :\n",
    "$$y_{j} = y_{j-1} + h \\times y'(t_{j-1}) $$\n",
    "Or, d'après l'équation différentielle $(E)$, on a $y'(t_{j-1}) = f(y(t_{j-1}),t_{j-1}) \\simeq f(y_{j-1},t_{j-1})$. On peut finalement donc écrire :\n",
    "$$y_{j} = y_{j-1} + h \\times f(y_{j-1},t_{j-1})$$\n",
    "\n",
    "En modélisation, pour retrouver cette formule (et en trouver d'autres plus complexes et/ou générales...), on préférera utiliser des D.L. : pour cela, on va exprimer $y_{j}$ en fonction de $y_{j-1}$, via un D.L. de $y(t_{j})$ en $t_{j-1}$. On a en effet :\n",
    "$$y(t_{j}) = y(t_{j-1}) + (t_{j}-t_{j-1}) \\times y'(t_{j-1}) + \\mathcal{o}(t_{j}-t_{j-1} ) =y(t_{j-1}) + h \\times y'(t_{j-1}) + \\mathcal{o}(h )$$\n",
    "En remplaçant $y'(t_{j-1})$ par $f(y(t_{j-1}),t_{j-1})=f(y_{j-1},t_{j-1})$, on trouve alors :\n",
    "$$y(t_{j}) = y(t_{j-1}) + h \\times f(y_{j-1},t_{j-1}) + \\mathcal{o}(h )$$\n",
    "\n",
    "On remarque alors qu'effectivement, l'expression approchée est bien :\n",
    "$$y_{j} \\simeq y_{j-1} + h \\times f(y_{j-1},t_{j-1})$$\n",
    "Cette expression devient exacte lorsque le pas $h$ tend vers 0, ce qui revient à prendre $n$ infiniment grand. Malheureusement, cela implique aussi un nombre de calculs pour l'ordinateur infiniment grand...\n",
    "\n",
    "Pour obtenir toutes les valeurs de la fonction $y$ sur l'intervalle discrétisé, il faut alors construire la suite des $y_j$ en utilisant la condition initiale : $y_0= y_i$. On peut alors construire la suite des $y_j$ de proche en proche, selon le schéma suivant :\n",
    "$$\\begin{cases}\n",
    "y_0 = y_i \\\\\n",
    "y_{j} = y_{j-1} + h \\times f(y_{j-1},t_{j-1}),\\ j \\in [\\![1,n ]\\!]\n",
    "\\end{cases}$$\n",
    "De même pour le temps :\n",
    "$$\\begin{cases}\n",
    "t_0 = t_i \\\\\n",
    "t_{j} = t_{j-1}+h,\\ j \\in [\\![1,n ]\\!]\n",
    "\\end{cases}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Testons sur l'exemple le plus simple cette méthode, pour cela on considère le problème de Cauchy suivant :\n",
    "$$\\begin{cases}\n",
    "y'(t)=y(t) \\\\\n",
    "y(0)=1\n",
    "\\end{cases}$$\n",
    "La solution de ce problème est naturellement la fonction exponentielle, mais effectuons la résolution numérique approchée en utilisant la méthode d'Euler.\n",
    "\n",
    "**Question 1 :** Ecrire la fonction `Euler(f,ti,tf,yi,n)` retournant la liste `T`, liste des temps, et `Y`, liste des valeurs approchées de la fonction y(t) aux temps correspondants, par la méthode d'Euler. Il est conseillé, dans un premier temps, de tenter de partir de la cellule quasi-vide ci-dessous. En cas de difficultés, compléter la cellule suivante."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Euler(f,ti,tf,yi,n):\n",
    "    @@@\n",
    "    return T,Y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def Euler(f,ti,tf,yi,n): # on suit toujours nos notations\n",
    "    # Pas de temps :\n",
    "    h = @@@\n",
    "    # Création des tableaux contenant pour l'instant des 0 pour le temps t et l'ordonnée y, pour les n+1 valeurs discrétisées\n",
    "    T, Y = @@@, @@@ \n",
    "    # C.I.:\n",
    "    T[0] = @@@   \n",
    "    Y[0] = @@@ \n",
    "    # Boucle d'\"hérédité\", soit la méthode d'Euler\n",
    "    for j in range(@@@):\n",
    "        T[j] = @@@ #tj en fonction de tj-1\n",
    "        Y[j] = @@@ # yj en fonction de yj-1\n",
    "    return T,Y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2 :** Ecrire la fonction `fonction_f(y,t)` correspondant au problème d'Euler décrit précedemment, puis utiliser la fonction Euler définie précédemment pour réaliser un graphe comparant la solution numérique par méthode d'Euler et la solution exacte du problème de Cauchy. On fera la résolution sur l'intervalle $[0,5]$, et on étudiera l'influence du choix de n :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Remarque :*\n",
    "La méthode utilisant un D.L. est plus générale, et permettra de trouver des méthodes similaires à celle d'Euler, mais pour des équations plus complexes (d'ordre 2, avec plusieurs variables). Pour cela, il est nécessaire de bien connaître la formule de Taylor-Young des D.L. :\n",
    "$$f(x+h) = f(x) + \\sum_{k = 1}^{n} \\frac{h^k}{k!}\\frac{\\mathrm{d} ^k f}{\\mathrm{d} x^k} (x)  + \\mathcal{O}(h^{n+1})$$\n",
    "Ou bien :\n",
    "$$f(x) = f(x_0) + \\sum_{k = 1}^{n} \\frac{(x-x_0)^k}{k!}\\frac{\\mathrm{d} ^k f}{\\mathrm{d} x^k} (x_0)  + \\mathcal{O}((x-x_0)^{n+1})$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Résolution d'un problème de physique\n",
    "\n",
    "On considère le circuit $RC$ suivant :\n",
    "\n",
    "![image](im/CircuitRC.png)\n",
    "\n",
    "**Question 1 :** Montrer que l'équation différentielle sur $u_C$ peut s'écrire :\n",
    "$$ \\frac{du (t)}{dt} + \\frac{u (t)}{\\tau} = \\frac{e(t)}{\\tau}$$\n",
    "Donner l'expression de $\\tau$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On cherche à trouver la solution approchée de cette équation différentielle à l'aide de la méthode d'Euler. Pour cela, on va considérer deux possibilités. La première va consister à réutiliser le travail fait précedemment, et utiliser la fonction `Euler` déjà écrite.\n",
    "\n",
    "**Question 2 :** Ecrire la fonction `fonction_f_RC(y,t)` dépendant de la fonction $e(t)$ (tension au bornes du générateur), permettant d'utiliser la fonction `Euler(f,ti,tf,yi,n)` pour résoudre ce problème."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fonction_f_RC(y,t):\n",
    "    return @@@"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3 :** On considère dans un premier temps que la source de tension génère un échelon de tension :\n",
    "$$ e(t) = \\begin{cases} 0 & \\mathrm{pour}\\  t\\leq0 \\\\ 10 & \\mathrm{pour}\\  t>0 \\end{cases}$$\n",
    "Ecrire la fonction `e(t)` retournant la valeur de $e(t)$ selon la fonction décrite ci-dessus."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def e(t):\n",
    "    @@@"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4 :** Pour cet échelon de tension, construire alors les listes `T` et `U` contenant 1001 valeurs de $t$ et $u_C(t)$ sur l'intervalle $I = [0,20]$ (en secondes), pour $\\tau = 3 s$ et avec la condition initiale $u_C (0)=0$. Tracer ensuite le graphique de $u_C(t)$ et $e(t)$ en fonction de $t$ sur l'intervalle $I$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tout ce travail n'a que peu d'intérêt théorique, car nous sommes capable de déterminer analytiquement la solution :\n",
    "$$ u_C (t) = 10 \\left (1- exp \\left ( - \\frac{t}{\\tau} \\right ) \\right )$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On peut adopter un second point de vue, en définissant une fonction Euler adaptée à ce problème spécifique (circuit RC), ne prenant pas en argument la fonction $f$ (second membre de l'équation différentielle), mais directement la fonction $e$. On en profite aussi pour passer $\\tau$ en argument.\n",
    "\n",
    "**Question 5 :** Ecrire la fonction `Euler_RC(e,ti,tf,ui,tau,n)` donnant, par la méthode d'Euler, une solution approchée de cette équation diférentielle. Cette fonction devra retourner deux listes `T` et `U` contenant les $n+1$ valeurs de $t$ et $u_C(t)$ sur l'intervalle $I=[t_i, t_f]$. `e` est la fonction de la variable `t` donnant la tension aux bornes du générateur $e(t)$ (ne pas l'écrire), `tau` le temps caractéristique du circuit et `ui` la condition initiale `ui`$=u_C(t_i)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Euler_RC(e,ti,tf,ui,tau,n):\n",
    "    @@@"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Intéressons-nous désormais à des problèmes plus interessants, pour lesquels nous ne sommes pas capables de déterminer la solution analytique. Essayons déjà avec un échelon \"lissé\".\n",
    "\n",
    "**Question 6 :** Définir une nouvelle fonction `echelon_lisse(t)` selon la fonction suivante :\n",
    "$$ e(t) = \\begin{cases} \n",
    "0 &\\mathrm{pour}\\   t < 0 \\\\ t^2 & \\mathrm{pour}\\ 0 \\leq t < \\sqrt{5}\\\\\n",
    "10-(t-2\\sqrt{5})^2  &\\mathrm{pour}\\ \\sqrt{5} \\leq t < 2 \\sqrt{5}\\\\\n",
    "10 &\\mathrm{pour}\\ 2 \\sqrt{5} \\leq t  \\end{cases}$$\n",
    "puis faire la résolution de l'équation différentielle via la méthode d'Euler. Tracer la solution obtenue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def echelon_lisse(t):\n",
    "    @@@"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Et maintenant pour un double échelon.\n",
    "\n",
    "**Question 7 :** Définir une nouvelle fonction `echelon_double(t)` selon la fonction suivante :\n",
    "$$ e(t) = \\begin{cases} \n",
    "0 &\\mathrm{pour}\\   t \\leq 0 \\\\ 5 & \\mathrm{pour}\\ 0 < t < 5\\\\\n",
    "10  &\\mathrm{pour}\\ 5 \\leq t  \\end{cases}$$\n",
    "puis faire la résolution de l'équation différentielle via la méthode d'Euler. Tracer la solution obtenue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def echelon_double(t):\n",
    "    @@@"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Equations différentielles d'ordre 2\n",
    "\n",
    "## Problème : le pendule\n",
    "\n",
    "**Question 1 :** Compléter :\n",
    "    \n",
    "- équation différentielle pour un pendule idéal *sans approximation des petits angles* :\n",
    "    \n",
    "- avec l'approximation des petits angles : \n",
    "    \n",
    "- solution, avec l'approximation, pour l'angle initial $\\theta_0$ et une vitesse initiale nulle : \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans la suite, nous allons résoudre numériquement l'équation du pendule sans approximation des petits angles, pour la valeur $\\omega_0= 1\\ rad.s^{-1}$.\n",
    "Pour cela, on ré-écrit l'équation différentielle du second ordre en deux équations du premier ordre :\n",
    "\n",
    "$$ \\begin{cases} \\dot{\\theta }(t)= \\omega (t)  \\\\ \n",
    "        \\dot{\\omega} (t)=-sin(\\theta (t)) \\end{cases}$$\n",
    "\n",
    "Ou, sous forme vectorielle, $ \\dot{Y} = F(Y,t)$ avec :\n",
    "\n",
    "$$ Y (t)= \\left( \\begin{aligned}\\theta (t) \\\\ \\omega (t)  \\end{aligned}\\right) \\mathrm{ et\\ } F(Y,t) = \\left(\\begin{aligned}  &\\omega (t) \\\\ \n",
    "&-sin(\\theta (t)) \\end{aligned}\\right) $$\n",
    "\n",
    "Le but de la suite de cette partie est de résoudre numériquement, avec une méthode d'Euler, ce problème entre les temps $t_i$ et $t_f$, en subdivisant cet intervalle en $n$ intervalles égaux.\n",
    "Pour cela , nous allons créer trois listes, de longueur *n+1* :\n",
    "- `T` permettra de stocker les valeurs du temps *t<sub>0</sub> = t<sub>i</sub>*, *t<sub>1</sub>*, *t<sub>2</sub>*, ... *t<sub>n</sub>* = *t<sub>f</sub>*\n",
    "- `Theta`  permettra de stocker les valeurs de l'angle $\\theta (t)$, au cours du temps $\\theta_0 = \\theta(t_0)$, $\\theta_1 = \\theta(t_1)$, $\\theta_2= \\theta(t_2)$, ... $\\theta_n = \\theta(t_n)$\n",
    "- `Omega`  permettra de stocker les valeurs de la pulsation $\\omega (t)$, au cours du temps $\\omega_0 = \\omega(t_0)$, $\\omega_1 = \\omega(t_1)$, $\\omega_2= \\omega(t_2)$, ... $\\omega_n = \\omega(t_n)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2 :**\n",
    "Ecrire la fonction `Pendule_Euler(theta_i,omega_i,ti,tf,n)` retournant les listes `T`, `Theta` et `Omega`, grâce à la méthode d'Euler, appliquée entre *t<sub>i</sub>* et *t<sub>f</sub>*, avec pour conditions initiales `theta_i` = $\\theta (t_i)$  et `omega_i` = $\\omega (t_i) = \\dot \\theta (t_i)$ ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Pendule_Euler(theta_i,omega_i,ti,tf,n):\n",
    "    @@@\n",
    "    return T,Theta,Omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparons avec la résolution numérique fournie avec la bibliothèque `scipy` (fonction `odeint`) et la solution exacte avec approximation des petits angles, pour les conditions initiales $\\theta_0 = \\frac{\\pi}{3}$ et une vitesse initiale nulle :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pend(y, t): # fonction définissant les deux équations différentielles couplées\n",
    "     theta, omega = y\n",
    "     dydt = [omega, -np.sin(theta)]\n",
    "     return dydt\n",
    "     \n",
    "# conditions initiales :\n",
    "theta_i=np.pi/3 # C.I. pour l'angle theta\n",
    "omega_i=0 # C.I. pour oméga, c'est-à-dire la vitesse angulaire.\n",
    "yi = [theta_i, omega_i] \n",
    "\n",
    "ti=0 # Début de l'intervalle de temps\n",
    "tf=6*pi # fin de l'intervalle de temps\n",
    "T=np.linspace(ti,tf,201) # liste des points de l'intervalle temporel de résolution\n",
    "\n",
    "ratio = 1.5 # ratio de taille entre fig et texte (légende et axes), par défaut 1\n",
    "plt.figure(figsize=(8*ratio,5*ratio),dpi = 200)\n",
    "# Graphe avec la fonction \"toute faite\" odeint\n",
    "plt.plot(T, odeint(pend,yi,T)[:,0],label=r'$ \\theta (t)$ avec odeint')\n",
    "\n",
    "# Graphe de la solution exacte avec approx des petits angles\n",
    "plt.plot(T, theta_i*np.cos(T),label=r'$ \\theta (t)$ exacte avec petits angles')\n",
    "\n",
    "#Graphe avec Pendule_Euler :\n",
    "T, Theta, Omega = Pendule_Euler(theta_i,omega_i,ti,tf,500)\n",
    "plt.plot(T,Theta,label=r'$ \\theta (t)$ avec Euler, n = 500')\n",
    "T, Theta, Omega = Pendule_Euler(theta_i,omega_i,ti,tf,2000)\n",
    "plt.plot(T, Theta,label=r'$ \\theta (t)$ avec Euler, n = 2000')\n",
    "\n",
    "plt.xlabel(\"t\")\n",
    "plt.title(\"Solution du pendule : angle en fonction du temps\")\n",
    "plt.legend()\n",
    "plt.show()  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans la suite, on va réaliser le portrait de phase du système pendule : cela consiste à tracer un graphique de la vitesse angulaire en fonction de l'angle. Ainsi, on placera en abscisse l'angle $\\theta$ et en ordonnée la vitesse angulaire $\\omega$, et on obtient la trajectoire dans l'espace des phases en faisant varier le temps.\n",
    "\n",
    "**Question 3 :**\n",
    "Ecrir un script permettant de réaliser le portrait de phase du système pendule, à partir de `ti=0`, pour les conditions initiales $\\theta_0 = \\frac{\\pi}{3}$ et une vitesse initiale nulle. On utilisera uniquement la fonction `Pendule_Euler`. Quel temps final `tf` faut-il considérer ?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4** : Interpréter la forme de la trajectoire observée, et donner le sens de parcours."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 5 :**  Ecrire la nouvelle fonction  `Pendule_Euler_f(theta_i,omega_i,ti,tf,f,n)` retournant les listes `T`, `Theta` et `Omega`, grâce à la méthode d'Euler, appliquée entre *t<sub>i</sub>* et *t<sub>f</sub>*, avec pour conditions initiales `theta_i` = $\\theta (t_i)$  et `omega_i` = $\\omega (t_i) = \\dot \\theta (t_i)$, en prenant cette fois-ci en compte des  frottements fluides (force de frottement de module $F=mfv$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'équation différentielle s'écrit alors ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Pendule_Euler_f(thetha_i,omega_i,ti,tf,f,n):\n",
    "    @@@\n",
    "    return T,Theta,Omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vérification du \"bon fonctionnement\" :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ti=0 # Début de l'intervalle de temps\n",
    "tf=6*pi # fin de l'intervalle de temps\n",
    "\n",
    "theta_i=np.pi/3 # C.I. pour l'angle theta\n",
    "omega_i=0 # C.I. pour oméga, c'est-à-dire la vitesse angulaire.\n",
    "\n",
    "ratio = 1 # ratio de taille entre fig et texte (légende et axes), par défaut 1\n",
    "plt.figure(figsize=(8*ratio,5*ratio),dpi = 200)\n",
    "\n",
    "T, Theta, Omega = Pendule_Euler_f(theta_0,omega_0,ti,tf,0.4,10000)\n",
    "plt.plot(T, Theta,label=r'$ \\theta (t)$ avec Euler, n = 10000')\n",
    "\n",
    "plt.xlabel(\"t\")\n",
    "plt.title(\"Solution du pendule : angle en fonction du temps, avec frottement fluide\")\n",
    "plt.legend()\n",
    "plt.show()  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 6 :** Tracer le nouveau portrait de phase, pour les mêmes conditions que dans la question 3, pour $f=0,2\\ s^{-1}$. Qu'observe-t-on ? Quel temps final `tf` faut-il considérer ?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le point de coordonnées (0,0) est nommé *attracteur*, car il semble attirer la trajectoire."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Pour aller plus loin ...**\n",
    "    \n",
    "**Question 7**  : En modifiant la vitesse initiale, montrer qu'il existe plusieurs attracteurs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problème : Régime transitoire pour un circuit RLC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On considère un circuit RLC série alimenté, à partir du temps $t=0$, par un GBF à la pulsation $\\omega$ : pour $t>0$, $v_e (t) = cos(\\omega t)$. La sortie de ce filtre étudiée est prise aux bornes du condensateur.\n",
    "\n",
    "![image](im/circuit.png)  \n",
    "\n",
    "\n",
    "**Question1 :** Ecrire la fonction permettant de résoudre numériquement, via une méthode d'Euler, l'équation différentielle sur la tension aux bornes du condensateur.\n",
    "\n",
    "*Conseil : Utiliser les variables $\\omega_0$ et $Q$ plutôt que $R$, $L$ et $C$.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2 : Régime transitoire en réponse à un échelon de tension**\n",
    "\n",
    "Tracer le régime transitoire observé aux bornes du condensateur, en réponse à un échelon de tension. Observer les différents régimes possibles, et tracer pour chacun le portrait de phase.\n",
    "\n",
    "*Aide : la fonction $v_e (t)$ représente une tension continue si on prend $\\omega=0$ !*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3 : Régime transitoire en réponse à une excitation sinusoïdale**\n",
    "\n",
    "Même question, mais pour $\\omega \\neq 0$ (cela représente des cas que nous ne savons pas résoudre théoriquement !)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
